#pragma once
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "Matrix.h"

matrix cudaMatrixTranspose(matrix matrix);
matrix cudaMatrixScalarProduct(matrix matrix, float factor);
matrix cudaMatrixSum(matrix firstMatrix, matrix secondMatrix);
matrix cudaMatrixProduct(matrix firstMatrix, matrix secondMatrix);

__global__ void matrixTransposeKernel(float *mat, unsigned int rows, unsigned int cols, float *res);
__global__ void matrixScalarProductKernel(float *mat, float scale, unsigned int rows, unsigned int cols, float *res);
__global__ void matrixSumKernel(float *a, float *b, unsigned int rows, unsigned int cols, float *res);
__global__ void matrixProductKernel(float *a, float *b, unsigned int a_rows, unsigned int a_cols, unsigned int b_rows, unsigned int b_cols,float *res);