#pragma once

typedef struct matrix {
	unsigned int cols;
	unsigned int rows;
	float *array;
} matrix;

matrix matrixZero(unsigned int rows, unsigned int cols);
matrix matrixIdentity(unsigned int rows, unsigned int cols);
matrix matrixRandom(unsigned int rows, unsigned int cols);
matrix matrixRandom(unsigned int rows, unsigned int cols, unsigned int seed);
float matrixGetValueAt(matrix mat, unsigned int x, unsigned int y);
void matrixSetValueAt(matrix mat, unsigned int x, unsigned int y, float value);