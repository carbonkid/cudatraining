#include <stdlib.h>
#include <time.h>

#include "Matrix.h"

float matrixGetValueAt(matrix mat, unsigned int x, unsigned int y){
	return mat.array[x*mat.cols + y];
}

void matrixSetValueAt(matrix mat, unsigned int x, unsigned int y, float value){
	if (x < mat.cols && y < mat.rows){
		mat.array[x*mat.cols + y] = value;
	}
}

matrix matrixZero(unsigned int rows, unsigned int cols){
	matrix result;
	result.rows = rows;
	result.cols = cols;
	result.array = (float*)malloc(sizeof(float) * rows * cols);

	for (unsigned int i = 0; i < rows; i++){
		for (unsigned int j = 0; j < cols; j++){
			matrixSetValueAt(result, i, j, 0);
		}
	}

	return result;
}

matrix buildRandomMatrix(unsigned int rows, unsigned int cols){
	matrix result;
	result.rows = rows;
	result.cols = cols;
	result.array = (float*)malloc(sizeof(float) * rows * cols);

	for (unsigned int i = 0; i < rows; i++){
		for (unsigned int j = 0; j < cols; j++){
			float randomValue = rand() % 10;
			matrixSetValueAt(result, i, j, randomValue);
		}
	}

	return result;
}

matrix matrixRandom(unsigned int rows, unsigned int cols, unsigned int seed){
	//set randomness seed
	srand(seed);
	return buildRandomMatrix(rows, cols);
}

matrix matrixRandom(unsigned int rows, unsigned int cols){
	//set randomness seed
	return matrixRandom(rows, cols, time(NULL));
}


matrix matrixIdentity(unsigned int rows, unsigned int cols){
	matrix result;
	result.rows = rows;
	result.cols = cols;
	result.array = (float*)malloc(sizeof(float) * rows * cols);

	for (unsigned int i = 0; i < rows; i++){
		for (unsigned int j = 0; j < cols; j++){
			if (i == j){
				matrixSetValueAt(result, i, j, 1);
			}else{
				matrixSetValueAt(result, i, j, 0);
			}
		}
	}

	return result;
}