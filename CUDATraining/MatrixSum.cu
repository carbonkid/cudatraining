#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "CUDAMatrix.cuh"

void printMatrix(matrix mat){
	for (int i = 0; i < mat.rows; i++) {
		for (int j = 0; j < mat.cols; j++) {
			float value = matrixGetValueAt(mat, i, j);
			printf("%d ", (int)value);
		}
		printf("\n");
	}
}


int main(int argc, char **argv){
	//create a matrix
	const unsigned int size = 1024;
	matrix firstMatrix = matrixRandom(size, size);
	matrix secondMatrix = matrixIdentity(size, size);

	//perform a parallel transposition
	matrix transposed = cudaMatrixTranspose(firstMatrix);

	//perform a parallel sum
	matrix sum = cudaMatrixSum(firstMatrix, secondMatrix);

	//perform a parallel row-column product
	matrix prod = cudaMatrixProduct(firstMatrix, sum);

	//print results
	/*
	printf("first matrix:\n");
	printMatrix(firstMatrix);
	printf("second matrix:\n");
	printMatrix(secondMatrix);
	printf("transposed first matrix:\n");
	printMatrix(transposed);
	printf("sum of two matrices:\n");
	printMatrix(sum);
	printf("product of two matrices:\n");
	printMatrix(prod);
	*/

	return EXIT_SUCCESS;
}
