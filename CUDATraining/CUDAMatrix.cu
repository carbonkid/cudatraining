#include "CUDAMatrix.cuh"
#include <stdio.h>
#include <math.h>

const unsigned int tileSize = 32;

matrix cudaMatrixTranspose(matrix mat){
	matrix result = matrixIdentity(mat.rows, mat.cols);
	size_t size = sizeof(float) * mat.cols * mat.rows;
	float blockCountX = ceil((float)mat.cols / (float)tileSize);
	float blockCountY = ceil((float)mat.rows / (float)tileSize);
	dim3 gridSize = { (unsigned int)ceil(blockCountX), (unsigned int)ceil(blockCountY) };
	dim3 blockSize = { tileSize, tileSize };
	float *dMatrix, *dResult;

	cudaMalloc(&dMatrix, size);
	cudaMalloc(&dResult, size);
	cudaMemcpy(dMatrix, mat.array, size, cudaMemcpyHostToDevice);
	matrixTransposeKernel<<<gridSize, blockSize>>>(dMatrix, mat.rows, mat.cols, dResult);
	cudaThreadSynchronize();
	cudaMemcpy(result.array, dResult, size, cudaMemcpyDeviceToHost);
	cudaFree(dMatrix);
	cudaFree(dResult);
	
	return result;
}



matrix cudaMatrixScalarProduct(matrix mat, float factor){
	matrix result = matrixZero(mat.rows, mat.cols);
	return result;
}



matrix cudaMatrixSum(matrix firstMatrix, matrix secondMatrix){
	matrix result = matrixZero(firstMatrix.rows, firstMatrix.cols);
	size_t size = sizeof(float) *firstMatrix.cols * secondMatrix.rows;
	float blockCountX = ceil((float)firstMatrix.cols / (float)tileSize);
	float blockCountY = ceil((float)firstMatrix.rows / (float)tileSize);
	dim3 gridSize = { (unsigned int)ceil(blockCountX), (unsigned int)ceil(blockCountY) };
	dim3 blockSize = { tileSize, tileSize };
	float *dFirstMatrix, *dSecondMatrix, *dResult;

	cudaMalloc(&dFirstMatrix, size);
	cudaMalloc(&dSecondMatrix, size);
	cudaMalloc(&dResult, size);
	cudaMemcpy(dFirstMatrix, firstMatrix.array, size, cudaMemcpyHostToDevice);
	cudaMemcpy(dSecondMatrix, secondMatrix.array, size, cudaMemcpyHostToDevice);
	matrixSumKernel<<<gridSize, blockSize>>>(dFirstMatrix, dSecondMatrix, firstMatrix.rows, firstMatrix.cols, dResult);
	cudaThreadSynchronize();
	cudaMemcpy(result.array, dResult, size, cudaMemcpyDeviceToHost);
	cudaFree(dFirstMatrix);
	cudaFree(dSecondMatrix);
	cudaFree(dResult);

	return result;
}



matrix cudaMatrixProduct(matrix firstMatrix, matrix secondMatrix){
	matrix result = matrixZero(firstMatrix.rows, secondMatrix.cols);
	size_t sizeFirst = sizeof(float) * firstMatrix.rows * firstMatrix.cols;
	size_t sizeSecond = sizeof(float) * secondMatrix.rows * secondMatrix.cols;
	size_t sizeResult = sizeof(float) * firstMatrix.rows * secondMatrix.cols;


	float blockCountX = ceil((float)secondMatrix.cols / (float)tileSize);
	float blockCountY = ceil((float)firstMatrix.rows / (float)tileSize);
	dim3 gridSize = { (unsigned int)ceil(blockCountX), (unsigned int)ceil(blockCountY) };
	dim3 blockSize = { tileSize, tileSize };
	float *dFirstMatrix, *dSecondMatrix, *dResult;

	cudaMalloc(&dFirstMatrix, sizeFirst);
	cudaMalloc(&dSecondMatrix, sizeSecond);
	cudaMalloc(&dResult, sizeResult);
	cudaMemcpy(dFirstMatrix, firstMatrix.array, sizeFirst, cudaMemcpyHostToDevice);
	cudaMemcpy(dSecondMatrix, secondMatrix.array, sizeSecond, cudaMemcpyHostToDevice);
	matrixProductKernel<<<gridSize, blockSize >>>(dFirstMatrix, dSecondMatrix, firstMatrix.rows, firstMatrix.cols, secondMatrix.rows, secondMatrix.cols, dResult);
	cudaThreadSynchronize();
	cudaMemcpy(result.array, dResult, sizeResult, cudaMemcpyDeviceToHost);
	cudaFree(dFirstMatrix);
	cudaFree(dSecondMatrix);
	cudaFree(dResult);

	return result;
}



__global__ void matrixTransposeKernel(float *mat, unsigned int rows, unsigned int cols, float *res){
	const unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
	const unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;

	if (x < rows && y < cols){
		res[x*cols + y] = mat[y*cols + x];
	}
	
}



__global__ void matrixScalarProductKernel(float *mat, float scale, unsigned int rows, unsigned int cols, float *res){
	const unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
	const unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;

	if (x < rows && y < cols){
		res[x*cols + y] = mat[y*cols + x] * scale;
	}
}



__global__ void matrixSumKernel(float *a, float *b, unsigned int rows, unsigned int cols, float *res){
	const unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
	const unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;

	if (x < rows && y < cols){
		res[x*cols + y] = a[x*cols + y] + b[x*cols + y];
	}
}



__global__ void matrixProductKernel(float *a, float *b, 
	unsigned int a_rows, unsigned int a_cols, unsigned int b_rows, unsigned int b_cols, float *res){
	const unsigned int x = (blockIdx.x * blockDim.x) + threadIdx.x;
	const unsigned int y = (blockIdx.y * blockDim.y) + threadIdx.y;

	if (x < a_rows && y < b_cols){
		float value = 0;

		for (int i = 0; i < a_cols; i++){
			value += a[x*a_cols + i] * b[i*b_cols + y];
		}

		res[x*b_cols + y] = value;
	}
}